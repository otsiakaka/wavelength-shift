#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
author: Olivier Tsiakaka
date: 17/04/2020

Simulation de la variation du SpO2 selon la largeur spectrale des sources
et des photocapteurs
Model de distribution gaussien
"""

import matplotlib.pyplot as plt
import numpy as np
import numpy.ma as ma
import csv
from scipy.interpolate import interp1d

plt.close('all')
plt.rcParams.update({'font.size':20})

###############################################################################
def Gamma2sigma(Gamma):
    '''Function to convert FWHM (Gamma) to standard deviation (sigma)'''
    return Gamma / (2 * np.sqrt(2 * np.log(2)))

def spectrumNorm(x, mu, gam):
    '''Creates the normalized LED spectrum curve from classical datasheet parameters'''
    return np.exp(-((x - mu)**2) / (2 * Gamma2sigma(gam)**2))

def approximateSpO2(R):
    """ Calcul le SpO2 selon la régression linéaire pour 660-940
    """
    return 110 - 25 * R

def absoluteSpO2(R, E_Hb_R, E_HbO2_R, E_Hb_IR, E_HbO2_IR):
    """ Calcul le SpO2% selon la methode empirique avec les valeurs
        des coefficients d'extinction et le Ratio of Ratios R
    """
    return 100*(E_Hb_R-R*E_Hb_IR) / (E_Hb_R-E_HbO2_R+R*(E_HbO2_IR-E_Hb_IR))
###############################################################################

with open('./spectra/hemoglobins/deoxyhemoglobin_extinctionOT.csv', 'r') as f:
    reader = csv.reader(f, delimiter=',')
    L_Hb=[]
    E_Hb_raw=[]
    for row in reader:
        if reader.line_num < 21:
            continue
        L_Hb.append(float(row[0]))
        E_Hb_raw.append(float(row[1]))

with open('./spectra/hemoglobins/oxyhemoglobin_extinctionOT.csv', 'r') as f:
    reader = csv.reader(f, delimiter=',')
    L_HbO2=[]
    E_HbO2_raw=[]
    for row in reader:
        if reader.line_num < 21:
            continue
        L_HbO2.append(float(row[0]))
        E_HbO2_raw.append(float(row[1]))
# L_Hb identique L_HbO2 [250:5:1000] avec pas de 1 au lieu de 5 fournis in file
L = np.linspace(min(L_Hb),max(L_Hb),num=max(L_Hb)-min(L_Hb)+1,endpoint=True) # restricted lambda vector from data file [250:1:1000]
# need to fill in the gaps
finterpHb = interp1d(L_Hb,E_Hb_raw,kind='cubic')
E_Hb = finterpHb(L)
finterpHbO2 = interp1d(L_HbO2,E_HbO2_raw,kind='cubic')
E_HbO2 = finterpHbO2(L)

fig1 = plt.figure()
ax1 = fig1.add_subplot(111)

ax1.semilogy(L,E_HbO2,'b', label='HbO2')
ax1.semilogy(L,E_Hb,'r', label='Hb')
ax1.set_title("Log Extinction Coefficients")
ax1.set_xlabel("Wavelength [nm]")
ax1.set_ylabel("Molar Extinction Coefficient [cm^-1 (moles/l)^-1]")
ax1.legend(loc=0)
ax1.grid(True)

R = np.linspace(0, 5, num=100)
x = np.linspace(0, 1500, num=1501)
L_ext = ma.masked_outside(x, L[0], L[-1], copy=True) # extended lambda vector to match x vector for sources
E_Hb_ext = L_ext.copy()
np.place(E_Hb_ext, ~L_ext.mask, E_Hb)
E_HbO2_ext = L_ext.copy()
np.place(E_HbO2_ext, ~L_ext.mask, E_HbO2)


### Photodiode spectral responsivity model = (lambda, fwhm)
s_PD = spectrumNorm(L_ext, 770, 500)
#plot(x, PD, 'k--', label="TSL12")

pairs = [ #format Lambda1, fwhm1, lambda2, fwhm2
        (660, 1, 940, 1, "Ideal"), #Ideal 1,1
#        (660, 1, 940, 50, "1,50"),
#        (660, 50, 940, 1, "50,1"),
        (660, 20, 940, 50, "Generic"), # Generic 660/940, 20/50
        (662, 16, 934, 57, "VSMD66694"), #VSMD 662,16, 934,57
        (657, 16, 883, 46, "SMT660/890"), #SMT 657,16, 883,46
        (664, 15, 954, 44, "SFH7050"), #SFH 664,15, 954,44
        (660, 21, 899, 68, "LED#1"), #Mindray 660,21, 899,68
        (664, 22, 895, 62, "LED#2")] #Drager 664,22, 895,62
names = [e[4] for e in pairs]
#L_eff=[]
E_Hb_eff=[] # contient les epsilons effectifs des sources en tuples format (R,IR)
E_HbO2_eff=[] # contient les epsilons effectifs des sources en tuples format (R,IR)
Delta_E_Hb_eff = [] # contiendra les delta de valeurs par rapport a l'ideal
Delta_E_HbO2_eff = [] # contiendra les delta de valeurs par rapport a l'ideal

fig2 = plt.figure()
ax2 = fig2.add_subplot(111)
#ax2.grid(True)
#ax2.set_title("Absolute vs linear approx")
ax2.set_xlabel(r'$S_{p}O_{2}$[%]')
ax2.set_ylabel(r'$R$')
ax2.set_xlim(90,100)
ax2.set_ylim(0.2,0.9)
ax2.plot(approximateSpO2(R),R, 'k', label="Approx 110-25*R", linewidth=2)


for i,p in enumerate(pairs):
    L0_R, fwhm_R, L0_IR, fwhm_IR, name = p[0], p[1], p[2], p[3], p[4]
    s_R = spectrumNorm(L_ext, L0_R, fwhm_R)
    s_IR = spectrumNorm(L_ext, L0_IR, fwhm_IR)
#    plt.figure()
#    plt.plot(L_ext, s_R, 'r')
#    plt.plot(L_ext, s_R*s_PD, 'r*')
#    plt.plot(L_ext, s_IR, 'g')
#    plt.plot(L_ext, s_IR*s_PD, 'g*')
    #EFFECTIVE sepctra
    s_Reff = s_R*s_PD
    s_IReff = s_IR*s_PD
    #THEO
    E_Hb_R_theo= E_Hb_ext[int(L0_R)]
    E_HbO2_R_theo = E_HbO2_ext[int(L0_R)]
    E_Hb_IR_theo= E_Hb_ext[L0_IR]
    E_HbO2_IR_theo = E_HbO2_ext[int(L0_IR)]
    #REAL
    E_Hb_R= E_Hb_ext[s_Reff.argmax()]
    E_HbO2_R = E_HbO2_ext[s_Reff.argmax()]
    E_Hb_IR= E_Hb_ext[s_IReff.argmax()]
    E_HbO2_IR = E_HbO2_ext[s_IReff.argmax()]
    E_Hb_eff.append((E_Hb_R, E_Hb_IR))
    E_HbO2_eff.append((E_HbO2_R, E_HbO2_IR))
    print("***VERIFICATION DU SHIFT APRES APPLICATION DU SPECTRE PD pour R={}nm et IR={}nm***".format(L0_R, L0_IR))
    print("Theory RED E_Hb = {}, E_HbO2 = {})".format(E_Hb_R_theo, E_HbO2_R_theo))
    print("Theory IR E_Hb = {}, E_HbO2 = {})".format(E_Hb_IR_theo, E_HbO2_IR_theo))
    print("Effective RED E_Hb = {}, E_HbO2 = {})".format(E_Hb_R, E_HbO2_R))
    print("Effective IR E_Hb = {}, E_HbO2 = {})".format(E_Hb_IR, E_HbO2_IR))
    spo2 = absoluteSpO2(R, E_Hb_R, E_HbO2_R, E_Hb_IR, E_HbO2_IR)
    ax2.plot(spo2, R, label=name, linewidth=2, ls='dotted')
ax2.legend(loc=0)
plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)
# remplissage tuples de differences avec valeur ideale
for e in E_Hb_eff:
    Delta_E_Hb_eff.append((E_Hb_eff[0][0]-e[0],E_Hb_eff[0][1]-e[1]))

for e in E_HbO2_eff:
    Delta_E_HbO2_eff.append((E_HbO2_eff[0][0]-e[0],E_HbO2_eff[0][1]-e[1]))





fig3 = plt.figure()
ax31 = fig3.add_subplot(121)
#ax31.grid(True)
#ax31.set_title("Deviation from standars 660/940 pair related coefficients")
#ax3.set_xlabel(r'$S_{p}O_{2}$[%]')
ax31.set_ylabel(r'$\Delta \varepsilon_{Hb} \quad [L.mol^{-1}.cm^{-1}]$')
ax31.plot(names[1:],Delta_E_Hb_eff[1:], 'o', markersize=10)
plt.axhline(y=0, linewidth=1, linestyle='--', color='b')
plt.xticks(rotation=45, ha='right')
plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)

ax32 = fig3.add_subplot(122)
#ax32.grid(True)
ax32.set_ylabel(r'$\Delta \varepsilon_{HbO_2} \quad [L.mol^{-1}.cm^{-1}]$')
ax32.plot(names[1:],Delta_E_HbO2_eff[1:], 'o', markersize=10)
plt.axhline(y=0, linewidth=1, linestyle='--', color='b')
plt.xticks(rotation=45, ha='right')
plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)

plt.show()
