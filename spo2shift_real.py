#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
author: Olivier Tsiakaka
date: 17/04/2020

Simulation de la variation du SpO2 selon la largeur spectrale des sources
et des photocapteurs
Modèle réel, spectres collectés au spectrophotomètre
"""

import matplotlib.pyplot as plt
import numpy as np
import csv
from scipy.interpolate import interp1d
import os

plt.close('all')
plt.rcParams.update({'font.size':12})

###############################################################################
def approximateSpO2(R):
    """ Calcul le SpO2 selon la régression linéaire pour 660-940
    """
    return 110 - 25 * R

def absoluteSpO2(R, E_Hb_R, E_HbO2_R, E_Hb_IR, E_HbO2_IR):
    """ Calcul du SpO2% selon la methode empirique avec les valeurs de Prahl
        des coefficients d'extinction et le Ratio of Ratios R
    """
    return 100*(E_Hb_R-R*E_Hb_IR) / (E_Hb_R-E_HbO2_R+R*(E_HbO2_IR-E_Hb_IR))

def percenterror(bll, approx):
    """ Calcul du pourcentage d'erreur relative en considérant que la valeur
        absolue du spo2 est donnée par la BLL et que l'approx produit une
        erreur à quantifier
    """
    return 100*(abs(bll-approx)/bll)

def absoluterror(bll, approx):
    """ Calcul de l'erreur absolue en considérant que la valeur
        theorique du spo2 est donnée par la BLL et que l'approx produit une
        erreur à quantifier
    """
    return abs(bll-approx)
###############################################################################

with open('./spectra/hemoglobins/deoxyhemoglobin_extinctionOT.csv', 'r') as f:
    reader = csv.reader(f, delimiter=',')
    L_Hb=[]
    E_Hb_raw=[]
    for row in reader:
        if reader.line_num < 21:
            continue
        L_Hb.append(float(row[0]))
        E_Hb_raw.append(float(row[1]))

with open('./spectra/hemoglobins/oxyhemoglobin_extinctionOT.csv', 'r') as f:
    reader = csv.reader(f, delimiter=',')
    L_HbO2=[]
    E_HbO2_raw=[]
    for row in reader:
        if reader.line_num < 21:
            continue
        L_HbO2.append(float(row[0]))
        E_HbO2_raw.append(float(row[1]))
# L_Hb identique à L_HbO2 [250:1000] avec pas de 5nm
# remettre L sur un pas de 0.5nm
L = np.arange(334, 1000, 0.5) # restricted lambda vector from data file
finterpHb = interp1d(L_Hb,E_Hb_raw,kind='cubic')
E_Hb = finterpHb(L)
finterpHbO2 = interp1d(L_HbO2,E_HbO2_raw,kind='cubic')
E_HbO2 = finterpHbO2(L)

plt.figure()
plt.semilogy(L,E_HbO2,'b', label='HbO2', linewidth=3)
plt.semilogy(L,E_Hb,'r', label='Hb', linewidth=3)
plt.xlabel("Wavelength [nm]")
plt.ylabel(r'Molar Extinction Coefficient $[L.mol^{-1}.cm^{-1}]$')
plt.legend(loc=0)
plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)
##########################
with open('./spectra/PDs/FDS100.csv', 'r') as f:
    reader = csv.reader(f, delimiter=',')
    L_FDS100=[]
    R_FDS100=[]
    for row in reader:
        if reader.line_num < 7:
            continue
        L_FDS100.append(float(row[0]))
        R_FDS100.append(float(row[1]))

with open('./spectra/PDs/FDS1010.csv', 'r') as f:
    reader = csv.reader(f, delimiter=',')
    L_FDS1010=[]
    R_FDS1010=[]
    for row in reader:
        if reader.line_num < 7:
            continue
        L_FDS1010.append(float(row[0]))
        R_FDS1010.append(float(row[1]))
        
with open('./spectra/PDs/FDS025.csv', 'r') as f:
    reader = csv.reader(f, delimiter=',')
    L_FDS025=[]
    R_FDS025=[]
    for row in reader:
        if reader.line_num < 7:
            continue
        L_FDS025.append(float(row[0]))
        R_FDS025.append(float(row[1]))
##########################
plt.figure()
R_FDS100 = np.array(R_FDS100)/max(R_FDS100)
R_FDS1010 = np.array(R_FDS1010)/max(R_FDS1010)
R_FDS025 = np.array(R_FDS025)/max(R_FDS025)
plt.plot(L_FDS100, R_FDS100, label='FDS100', linewidth=3)
plt.plot(L_FDS1010, R_FDS1010, label='FDS1010', linewidth=3)
plt.plot(L_FDS025, R_FDS025, label='FDS025', linewidth=3)
plt.legend()
plt.xlabel(r'$\lambda (nm)$')
plt.ylabel('Normalized Relative Sensitivity')
plt.legend(loc=0)
plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)

finterpFDS100 = interp1d(L_FDS100, R_FDS100,  kind='linear', fill_value="extrapolate")
finterpFDS1010 = interp1d(L_FDS1010, R_FDS1010,  kind='linear', fill_value="extrapolate")
finterpFDS025 = interp1d(L_FDS025, R_FDS025,  kind='linear', fill_value="extrapolate")
R_FDS100 = finterpFDS100(L)
R_FDS1010 = finterpFDS1010(L)
R_FDS025 = finterpFDS025(L)
PDs = [("FDS100", R_FDS100),
       ("FDS1010", R_FDS1010),
       ("FDS025", R_FDS025)]

#plt.figure()
#plt.plot(L, R_FDS100/R_FDS100.max(), label='FDS100', linewidth=3)
#plt.plot(L, R_FDS1010/R_FDS1010.max(), label='FDS1010', linewidth=3)
#plt.plot(L, R_FDS025/R_FDS025.max(), label='FDS025', linewidth=3)
#plt.legend()
#plt.xlabel(r'$\lambda (nm)$')
#plt.ylabel('Normalized Relative Sensitivity')
###########################################
pairs = [('LHQ974', 'SFH4248', 'Typical'),
         ('VSMD66694-R', 'VSMD66694-IR', 'VSMD66694'),
         ('SMT660-890-R', 'SMT660-890-IR', 'SMT660/890'),
         ('SFH7050-R', 'SFH7050-IR', 'SFH7050'),
         ('Mindray-R', 'Mindray-IR', 'LED#1'),
         ('Drager-R', 'Drager-IR', 'LED#2')
         ]

dtype1 = np.dtype([('lambda', 'f8'), ('sample', 'f8')])

#### IDEAL VALUES AT 660nm and 940nm
## Take advantage L and E_Hbx vector are indexed identically
E_Hb_R_theo = E_Hb[np.where(L==660)[0][0]]
E_HbO2_R_theo = E_HbO2[np.where(L==660)[0][0]]
E_Hb_IR_theo = E_Hb[np.where(L==940)[0][0]]
E_HbO2_IR_theo = E_HbO2[np.where(L==940)[0][0]]

R = np.linspace(0, 5, num=100) # pour les calculs spo2=f(R)

#### figure des courbes spo2 absolute vs approx
fig2, axes2 = plt.subplots(1, 3)
axes2[0].set_title("Absolute vs approx with "+PDs[0][0])
axes2[0].set_xlabel(r'$S_{p}O_{2}$[%]')
axes2[0].set_ylabel(r'$R$')
axes2[0].set_xlim(90,100)
axes2[0].set_ylim(0.25,0.9)
axes2[0].plot(approximateSpO2(R),R, 'k', label="Approx 110-25*R")
axes2[0].spines['top'].set_visible(False)
axes2[0].spines['right'].set_visible(False)

axes2[1].set_title("Absolute vs approx with "+PDs[1][0])
axes2[1].set_xlabel(r'$S_{p}O_{2}$[%]')
axes2[1].set_ylabel(r'$R$')
axes2[1].set_xlim(90,100)
axes2[1].set_ylim(0.25,0.9)
axes2[1].plot(approximateSpO2(R),R, 'k', label="Approx 110-25*R")
axes2[1].spines['top'].set_visible(False)
axes2[1].spines['right'].set_visible(False)

axes2[2].set_title("Absolute vs approx with "+PDs[2][0])
axes2[2].set_xlabel(r'$S_{p}O_{2}$[%]')
axes2[2].set_ylabel(r'$R$')
axes2[2].set_xlim(90,100)
axes2[2].set_ylim(0.25,0.9)
axes2[2].plot(approximateSpO2(R),R, 'k', label="Approx 110-25*R")
axes2[2].spines['top'].set_visible(False)
axes2[2].spines['right'].set_visible(False)

#### figure des delta epsilon
fig3, axes3 = plt.subplots(3, 2, sharex=True)
axes3[0,0].set_title("Delta epsilon with "+PDs[0][0])
axes3[0,0].set_ylabel(r'$\Delta \varepsilon_{Hb} \quad [L.mol^{-1}.cm^{-1}]$')
axes3[0,0].axhline(y=0, linewidth=1, linestyle='--', color='b')
axes3[0,0].spines['top'].set_visible(False)
axes3[0,0].spines['right'].set_visible(False)

axes3[1,0].set_title("Delta epsilon with "+PDs[1][0])
axes3[1,0].set_ylabel(r'$\Delta \varepsilon_{Hb} \quad [L.mol^{-1}.cm^{-1}]$')
axes3[1,0].axhline(y=0, linewidth=1, linestyle='--', color='b')
axes3[1,0].spines['top'].set_visible(False)
axes3[1,0].spines['right'].set_visible(False)

axes3[2,0].set_title("Delta epsilon with "+PDs[2][0])
axes3[2,0].set_ylabel(r'$\Delta \varepsilon_{Hb} \quad [L.mol^{-1}.cm^{-1}]$')
axes3[2,0].axhline(y=0, linewidth=1, linestyle='--', color='b')
axes3[2,0].spines['top'].set_visible(False)
axes3[2,0].spines['right'].set_visible(False)

#
axes3[0,1].set_title("Delta epsilon with "+PDs[0][0])
axes3[0,1].set_ylabel(r'$\Delta \varepsilon_{HbO_2} \quad [L.mol^{-1}.cm^{-1}]$')
axes3[0,1].axhline(y=0, linewidth=1, linestyle='--', color='b')
axes3[0,1].spines['top'].set_visible(False)
axes3[0,1].spines['right'].set_visible(False)

axes3[1,1].set_title("Delta epsilon with "+PDs[1][0])
axes3[1,1].set_ylabel(r'$\Delta \varepsilon_{HbO_2} \quad [L.mol^{-1}.cm^{-1}]$')
axes3[1,1].axhline(y=0, linewidth=1, linestyle='--', color='b')
axes3[1,1].spines['top'].set_visible(False)
axes3[1,1].spines['right'].set_visible(False)

axes3[2,1].set_title("Delta epsilon with "+PDs[2][0])
axes3[2,1].set_ylabel(r'$\Delta \varepsilon_{HbO_2} \quad [L.mol^{-1}.cm^{-1}]$')
axes3[2,1].axhline(y=0, linewidth=1, linestyle='--', color='b')
axes3[2,1].spines['top'].set_visible(False)
axes3[2,1].spines['right'].set_visible(False)

#### figure des courbes relatve error spo2 
fig4, axes4 = plt.subplots(1, 3)
axes4[0].set_title("Absolute error with "+PDs[0][0])
axes4[0].set_xlabel(r'$\Delta S_{p}O_{2}$[%]')
axes4[0].set_ylabel(r'$R$')
axes4[0].set_xlim(0,14)
axes4[0].set_ylim(0.25,0.9)
#axes4[0].plot(approximateSpO2(R),R, 'k', label="Approx 110-25*R")
axes4[0].spines['top'].set_visible(False)
axes4[0].spines['right'].set_visible(False)

axes4[1].set_title("Absolute error with "+PDs[1][0])
axes4[1].set_xlabel(r'$\Delta S_{p}O_{2}$[%]')
axes4[1].set_ylabel(r'$R$')
axes4[1].set_xlim(0,14)
axes4[1].set_ylim(0.25,0.9)
#axes4[1].plot(approximateSpO2(R),R, 'k', label="Approx 110-25*R")
axes4[1].spines['top'].set_visible(False)
axes4[1].spines['right'].set_visible(False)

axes4[2].set_title("Absolute error with "+PDs[2][0])
axes4[2].set_xlabel(r'$\Delta S_{p}O_{2}$[%]')
axes4[2].set_ylabel(r'$R$')
axes4[2].set_xlim(0,14)
axes4[2].set_ylim(0.25,0.9)
#axes4[2].plot(R,approximateSpO2(R), 'k', label="Approx 110-25*R")
axes4[2].spines['top'].set_visible(False)
axes4[2].spines['right'].set_visible(False)

plt.figure()

for i,p in enumerate(pairs):
    spectrum_R = np.loadtxt(os.path.join('./spectra/LEDs',p[0]+'.txt'), dtype=dtype1, delimiter=';',skiprows=8, usecols=(0,1))
    s_R_init = spectrum_R['sample']/spectrum_R['sample'].max() #normalization
    spectrum_IR = np.loadtxt(os.path.join('./spectra/LEDs',p[1]+'.txt'), dtype=dtype1, delimiter=';',skiprows=8, usecols=(0,1))
    s_IR_init = spectrum_IR['sample']/spectrum_IR['sample'].max() #normalization
    ## remise sur le vecteur lambda commun
    ## oui ça tronque les spectres legerement vers les IR
    finterp_R = interp1d(spectrum_R['lambda'], s_R_init, kind='linear')
    s_R = finterp_R(L)
    finterp_IR = interp1d(spectrum_IR['lambda'], s_IR_init, kind='linear')
    s_IR = finterp_IR(L)
    ######
    print("***** PAIR {}***** ".format(p))
    print("* SOURCE SEULE {} *".format(p[0]))
    print("Lambda peak = {}".format(L[s_R.argmax()]))
    print("Corresponding E_Hb = {}".format(E_Hb[s_R.argmax()]))
    print("Corresponding E_HbO2 = {}".format(E_HbO2[s_R.argmax()]))
    print("* SOURCE SEULE {} *".format(p[1]))
    print("Lambda peak = {}".format(L[s_IR.argmax()]))
    print("Corresponding E_Hb = {}".format(E_Hb[s_IR.argmax()]))
    print("Corresponding E_HbO2 = {}".format(E_HbO2[s_IR.argmax()]))
    ######
    plt.subplot(2,3,i+1)
    plt.xlabel(r'$\lambda (nm)$')
    plt.ylabel('Normalized Intensity')
#    plt.plot(spectrum_R['lambda'], s_R, 'r', label=p[0], linewidth=2)
#    plt.plot(spectrum_R['lambda'], s_IR, 'grey', label=p[1], linewidth=2)
    plt.plot(L, s_R, 'r', label=p[0], linewidth=2)
    plt.plot(L, s_IR, 'grey', label=p[1], linewidth=2)
    plt.legend(loc='upper left')
    plt.gca().spines['top'].set_visible(False)
    plt.gca().spines['right'].set_visible(False)
    for j,PD in enumerate(PDs):
        ##spectres effectifs
        s_Reff = s_R*PD[1]
        s_IReff = s_IR*PD[1]
        ### EFFECTIFS ###
        E_Hb_Reff = E_Hb[s_Reff.argmax()]
        E_HbO2_Reff = E_HbO2[s_Reff.argmax()]
        E_Hb_IReff = E_Hb[s_IReff.argmax()]
        E_HbO2_IReff = E_HbO2[s_IReff.argmax()]
        print("*** AVEC PHOTODETECTEUR {} ***".format(PD[0]))
#        print("Nouveau Lambda peak = {}".format(L[s_Reff.argmax()]))
        print("Delta Lambda peak = {}".format(660-L[s_Reff.argmax()]))
#        print("Corresponding E_Hb = {}".format(E_Hb_Reff))
        print("Delta E_Hb = {}".format(E_Hb_R_theo-E_Hb_Reff))
#        print("Corresponding E_HbO2 = {}".format(E_HbO2_Reff))
        print("Delta E_HbO2 = {}".format(E_HbO2_R_theo-E_HbO2_Reff))
#        print("Nouveau Lambda peak = {}".format(L[s_IReff.argmax()]))
        print("Delta Lambda peak = {}".format(940-L[s_IReff.argmax()]))
#        print("Corresponding E_Hb = {}".format(E_Hb_IReff))
        print("Delta E_Hb = {}".format(E_Hb_IR_theo-E_Hb_IReff))
#        print("Corresponding E_HbO2 = {}".format(E_HbO2_IReff))
        print("Delta E_HbO2 = {}".format(E_HbO2_IR_theo-E_HbO2_IReff))
        spo2 = absoluteSpO2(R, E_Hb_Reff, E_HbO2_Reff, E_Hb_IReff, E_HbO2_IReff)
        ### subplots selon le photodetecteur
#        axes[j,0].plot(spo2, R, label=p[0]+" "+p[1]+" "+PD[0], linewidth=2)
        axes2[j].plot(spo2, R, label=p[2], linewidth=2)
        axes2[j].legend(loc=0, ncol=2)
        # to save space only the first name is used, otherwise use " ".join(p)
        axes3[j,0].plot(p[2],E_Hb_Reff-E_Hb_R_theo, color='red', marker='o', markersize=10)
        axes3[j,0].plot(p[2],E_Hb_IReff-E_Hb_IR_theo, color='grey', marker='o', markersize=10)
        axes3[j,1].plot(p[2],E_HbO2_Reff-E_HbO2_R_theo, color='red', marker='o', markersize=10)
        axes3[j,1].plot(p[2],E_HbO2_IReff-E_HbO2_IR_theo, color='grey', marker='o', markersize=10)
        axes4[j].plot(absoluterror(spo2, approximateSpO2(R)), R, label=p[2], linewidth=2)
        axes4[j].legend(loc=0, ncol=1)
##




plt.setp(axes3[2,0].get_xticklabels(), rotation=45, ha='right')
plt.setp(axes3[2,1].get_xticklabels(), rotation=45, ha='right')

plt.show()
