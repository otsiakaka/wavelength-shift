# Description
These two routines are used to predict the wavelength shift occurring in pulse oximetry du to spectral pairing of the optoelectronic components and the issuing inaccuracy in the oxygen saturation linear approximation widely used (SpO2 = 110-25*R).
1. `spo2shift_gauss.py` uses gaussian models
2. `spo2shift_real.py` uses real component spectra

Hemoglobin absorption spectra as well as LED emission spectra and photodiode responsivity are available in the spectra directory.

# Citing
Notice that the use of theses scripts and the dataset requires appropriate citation to the publication: Olivier Tsiakaka, Benoit Gosselin and Sylvain Feruglio, [*"Source-detector spectral pairing related inaccuracies in pulse oximetry: Evaluation of the wavelength shift"*](https://www.mdpi.com/1424-8220/20/11/3302), Sensors (2020).

# Requirements
* Developped with Python 3.7.6, Numpy 1.17.4, Matplotlib. 3.1.3
* Emission spectra collected with Avantes AvaSpec-ULS2048XL-USB2 spectrophotometer
* Photodiode spectra from Thorlabs http://www.thorlabs.com/newgrouppage9.cfm?objectgroup_id=285
* Hemoglobin spectra from Scott Prahl http://omlc.org/spectra/hemoglobin/index.html
